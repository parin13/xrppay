class UserExceptionStr():
    """
    Raw Data for End User Information/Exception
    """
    some_error_occurred = 'Some Error Occurred'
    bad_request = 'Bad Request!'
    specify_required_fields = 'Please specify all required fields!'
    input_params_wrong = 'Input Parameters are wrong.'
    insufficient_funds = 'Insufficient funds'
    invalid_user = 'Invalid User'
    not_user_address = 'This Address does not correspond to the User'
    nodejs_server_down = 'Node JS Server is Down'
    enc_error = 'Encryption Error'
    wrapcore_exception = 'Ripple Server not responding'
    address_exception = 'invalid address'
    node_error = 'Node Error'
    unknown_exception = 'Unknown Exception'

    success = 'Success!'
    bad_request = 'Bad Request!'
    invalid_user = 'Invalid User!'
    user_not_found = 'User not found'
    user_already_exist = 'User already exist!'
    some_error_occurred = 'Some Error Occurred!'
    incorrect_user_pass = 'Incorrect User Name or Password'
    specify_required_fields = 'Please specify all required fields!'
    server_not_responding = 'Server Not Responding. Try Again Later!'
    address_not_owner = 'This address does not correspond to the user.'
    address_not_active_incorrect = "Address is not active or incorrect!"
    error_request_data = 'Error while getting Request Data'