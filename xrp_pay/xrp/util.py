import models
import exception_str
import custom_exception
import common_util
import sys, os
from decimal import Decimal
import datetime
import ripple

# Log
log = 'end_points'
logs_directory, category = common_util.get_config(log)

# Common Methods for eth and erc
obj_common = common_util.CommonUtil(log=log)

# Drop to XRP
drop_to_xrp = 1000000

def get_balance(user_name, address):
    """
    Method for extracting balance from the response of get_account_info
    :param address:
    :return:
    """

    try:
        # Logging
        obj_logger = common_util.MyLogger(logs_directory, category)

        # Node URL
        node_url = common_util.config.get('node', 'url')

        # Check if the address correspond to the user
        if not models.find_sql(logger=obj_logger, table_name='address_master',filters={'user_name': user_name, 'address': address}):
            raise custom_exception.UserException(exception_str.UserExceptionStr.not_user_address)

        response = obj_common.rpc_request(node_url, 'account_info', [{"account": address}])

        if response:
            # Check if account is valid
            status = response.get('result',{}).get('status','')
            if status == 'success':
                balance = Decimal(response.get('result', {}).get('account_data', {}).get('Balance', 0))
                balance = str(balance / Decimal(drop_to_xrp))
                return balance
            elif status == 'error':
                raise custom_exception.UserException(response.get('result',{}).get('error_message',exception_str.UserExceptionStr.unknown_exception))
            else:
                raise custom_exception.UserException(exception_str.UserExceptionStr.some_error_occurred)
        else:
            raise custom_exception.UserException(exception_str.UserExceptionStr.address_not_active_incorrect)

    except custom_exception.UserException:
        raise

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | '  + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print error_stack
        obj_logger.error_logger('get_balance : ' + str(e))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


def generate_address(user_name, token):
    """
    Generating new address by RPC on the Ripple Node
    :return:
    """

    try:

        # Logging
        obj_logger = common_util.MyLogger(logs_directory, category)

        # Node URL
        node_url = common_util.config.get('node', 'url')

        # Generate Address RPC
        response = obj_common.rpc_request(node_url, 'wallet_propose', [])
        result = response.get('result',{})

        if result :
            address = str(result['account_id'])
            public_key = str(result['public_key'])

            master_seed = str(result['master_seed'])
            master_key = str(result['master_key'])

            # Encrypt Seeds
            enc_master_seed = common_util.AESCipher(token,log).encrypt(master_seed)
            enc_master_key = common_util.AESCipher(token,log).encrypt(master_key)

            # Check Encryption
            dec_master_seed = common_util.AESCipher(token,log).decrypt(enc_master_seed)
            dec_master_key = common_util.AESCipher(token, log).decrypt(enc_master_key)

            if (master_seed != dec_master_seed) or (master_key != dec_master_key):
                obj_logger.error_logger('Encryption Error')
                raise custom_exception.UserException(exception_str.UserExceptionStr.some_error_occurred)

            models.insert_sql(
                logger=obj_logger,
                table_name='address_master',
                data={
                    'user_name': user_name,
                    'address': address,
                    'master_seed': enc_master_seed,
                    'master_key': enc_master_key,
                    'public_key': public_key,
                    'timestamp': datetime.datetime.now()
                })

            # Add in Redis for Crawlers
            obj_common.get_redis_connection().sadd('xrp_aw_set', address)
            return address
        else:
            raise Exception(exception_str.UserExceptionStr.some_error_occurred)

    except Exception as e:

        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print error_stack
        obj_logger.error_logger('generate_address : ' + str(e))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


def get_fee():
    """
    RPC for getting fee
    :return:
    """
    try:

        # Node URL
        node_url = common_util.config.get('node', 'url')

        # Get Network Fee URL
        response = obj_common.rpc_request(node_url, 'fee', [])

        if not response:
            raise Exception('RPC Error')

        fee = response.get("result", {}).get('drops', {}).get("base_fee", 0)

        if not fee:
            raise Exception(exception_str.UserExceptionStr.server_not_responding)

        return str(Decimal(fee) / Decimal(drop_to_xrp))

    except Exception as e:
        obj_logger = common_util.MyLogger(logs_directory, category)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print error_stack
        obj_logger.error_logger('get_fee : ' + str(e))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


def sign_transaction(from_address, to_address, amount, secret_key):
    try :

        # method = 'sign'
        params = {
            "offline": False,
            "secret": secret_key,
            "tx_json": {
                "Account": from_address,
                "Amount" : str(int(amount * drop_to_xrp)),
                "Destination": to_address,
                "TransactionType": "Payment"
            },
        }

        transaction = {
                "Account": from_address,
                "Amount" : str(int(amount * drop_to_xrp)),
                "Destination": to_address,
                "TransactionType": "Payment"
            }

        # Node URL
        node_url = common_util.config.get('node', 'url')

        # Sign Transaction
        response = obj_common.rpc_request(node_url, 'sign', [params])

        if not response:
            raise Exception('RPC Error')

        if response.get('result',{}).get('status','') == 'error':
            raise Exception(response.get('result',{}).get('error_message',exception_str.UserExceptionStr.unknown_exception))

        tx_blob = response.get('result', {}).get('tx_blob', {})

        return tx_blob

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print(error_stack)
        obj_logger = common_util.MyLogger(logs_directory, category)
        obj_logger.error_logger('sign_transaction : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)

def forward_xrp(user_name, token, from_address, to_address, value):
    """
    To Transfer Ethereum
    """
    try:

        # value
        value = Decimal(value)

        # Get User Ether Balance in Wei
        balance = Decimal(get_balance(user_name, from_address))

        # Transaction Fee in
        tx_fee = Decimal(get_fee())

        # Check if the transaction fee > eth_balance
        if (tx_fee > balance) or (value + tx_fee > balance):
            raise custom_exception.UserException(exception_str.UserExceptionStr.insufficient_funds)

        # Decrypt Private Key
        obj_logger = common_util.MyLogger(logs_directory, category)
        enc_private_key = models.find_sql(logger=obj_logger, table_name='address_master', filters={'user_name': user_name, 'address':from_address})[0]['master_seed']
        if not enc_private_key:
            raise custom_exception.UserException(exception_str.UserExceptionStr.not_user_address)
        private_key = common_util.AESCipher(token, log).decrypt(enc_private_key)

        # Sign Transaction
        tx_blob = sign_transaction(from_address, to_address, value, private_key)

        # Node URL
        node_url = common_util.config.get('node', 'url')

        # Create Raw Transaction
        response = obj_common.rpc_request(node_url, 'submit', [{'tx_blob': tx_blob}])

        if response.get('result').get('engine_result') == 'tecNO_DST_INSUF_XRP' :
            raise custom_exception.UserException(exception_str.UserExceptionStr.insufficient_funds)

        tx_hash = response.get('result',{}).get('tx_json',{}).get('hash',{})

        if not tx_hash:
            raise custom_exception.UserException(exception_str.UserExceptionStr.some_error_occurred)

        return tx_hash

    except custom_exception.UserException:
        raise
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print(error_stack)
        obj_logger = common_util.MyLogger(logs_directory, category)
        obj_logger.error_logger('forward_xrp : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


def forward_batch(user_name, token, to_address, value):
    """
    To Transfer Ethereum
    """
    try:

        # logger
        obj_logger = common_util.MyLogger(logs_directory, category)

        # Getting data of from address
        try:
            from_address = common_util.config.get('forward', 'address')
            enc_private_key_path = common_util.config.get('forward', 'private_key')
            enc_private_key = None
            with open(enc_private_key_path, 'r') as obj_file:
                enc_private_key = obj_file.readlines()[0]  # private key should be in the first line
        except:
            raise custom_exception.UserException(exception_str.UserExceptionStr.private_key_not_found)

        if not enc_private_key:
            raise custom_exception.UserException(exception_str.UserExceptionStr.private_key_not_found)

        # Decrypt Private Key
        private_key = common_util.AESCipher(token, log).decrypt(enc_private_key)

        # value
        value = Decimal(value)

        # Get User Ether Balance in Wei
        balance = Decimal(get_balance(user_name, from_address))

        # Transaction Fee in
        tx_fee = Decimal(get_fee())

        # Check if the transaction fee > eth_balance
        if (tx_fee > balance) or (value + tx_fee > balance):
            raise custom_exception.UserException(exception_str.UserExceptionStr.insufficient_funds)

        # Sign Transaction
        tx_blob = sign_transaction(from_address, to_address, value, private_key)

        # Node URL
        node_url = common_util.config.get('node', 'url')

        # Create Raw Transaction
        response = obj_common.rpc_request(node_url, 'submit', [{'tx_blob': tx_blob}])

        if response.get('result').get('engine_result') == 'tecNO_DST_INSUF_XRP' :
            raise custom_exception.UserException(exception_str.UserExceptionStr.insufficient_funds)

        tx_hash = response.get('result',{}).get('tx_json',{}).get('hash',{})

        if not tx_hash:
            raise custom_exception.UserException(exception_str.UserExceptionStr.some_error_occurred)

        return tx_hash

    except custom_exception.UserException:
        raise
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print(error_stack)
        obj_logger.error_logger('forward_batch : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)