# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import util
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import common_util
import json
import exception_str
import custom_exception

obj_common = common_util.CommonUtil(log=util.log)

@csrf_exempt
@obj_common.who_is_hitting
@obj_common.valid_user
def generate_address(request):
    """
    End point for generating new address
    :param request:
    :return: address
    """
    if request.method == 'POST':
        try:
            # Get Request Data
            post_data = json.loads(request.body)
            user_name = post_data.get("user_name", '')
            token = post_data.get("token", '')

            # Generate Address
            address = util.generate_address(user_name, token)

            # Success
            return JsonResponse({'address': address,'status' : 200})

        except custom_exception.UserException as e:
            return JsonResponse({'error': str(e), 'status': 400})
        except Exception as e:
            util.init_logger()
            util.logger.info("Error generate_new_address : " + str(e))
            return JsonResponse({'error': exception_str.UserExceptionStr.bad_request,'status' : 400})


@csrf_exempt
@obj_common.who_is_hitting
@obj_common.valid_user
def get_balance(request):
    """
    End point for getting balance
    :param request:
    :return: balance
    """
    if request.method == 'POST':
        try:

            # Get Request Params
            post_data = json.loads(request.body)
            user_name = post_data.get("user_name", '')
            address = post_data.get("address", '')

            # Server Side Checks
            common_util.check_if_present(user_name, address)

            # Get Balance
            balance = util.get_balance(user_name, address)

            return JsonResponse({'balance': balance, 'status': 200})

        except custom_exception.UserException as e:
            return JsonResponse({'error': str(e), 'status': 400})
        except Exception as e:
            util.init_logger()
            util.logger.info("Error get_balance : " + str(e))
            return JsonResponse({'error': exception_str.UserExceptionStr.bad_request,'status' : 400})


@csrf_exempt
@obj_common.who_is_hitting
@obj_common.valid_user
def get_fee(request):
    """
    End point for getting fee for normal transaction
    :param request:
    :return: fee in drops
    """
    if request.method == 'POST':
        try:
            fee = util.get_fee()
            return JsonResponse({'fee': fee, 'status': 200})
        except custom_exception.UserException as e:
            return JsonResponse({'error': str(e), 'status': 400})
        except Exception as e:
            util.init_logger()
            util.logger.info("Error get_fee : " + str(e))
            return JsonResponse({'error': exception_str.UserExceptionStr.bad_request, 'status' : 400})


@csrf_exempt
@obj_common.who_is_hitting
@obj_common.valid_user
def forward_xrp(request):
    """
    End point for getting fee for normal transaction
    :param request:
    :return: fee in drops
    """
    if request.method == 'POST':
        try:

            # Get Request Data
            post_data = json.loads(request.body)
            user_name = post_data.get("user_name", '')
            token = post_data.get("token", '')
            from_address = post_data.get("from_address", '')
            to_address = post_data.get("to_address", '')
            value = post_data.get("value", '')

            # Server Side Checks
            common_util.check_if_present(user_name, token, from_address, to_address, value)

            # Generate Address
            tx_hash = util.forward_xrp(user_name, token, from_address, to_address, value)

            # Success
            return JsonResponse({'tx_hash': tx_hash, 'status': 200})

        except custom_exception.UserException as e:
            return JsonResponse({'error': str(e), 'status': 400})
        except Exception as e:
            util.init_logger()
            util.logger.info("Error get_fee : " + str(e))
            return JsonResponse({'error': exception_str.UserExceptionStr.bad_request, 'status' : 400})


@csrf_exempt
@obj_common.who_is_hitting
@obj_common.valid_user
def forward_batch(request):
    """
    End point for getting fee for normal transaction
    :param request:
    :return: fee in drops
    """
    if request.method == 'POST':
        try:

            # Get Request Data
            post_data = json.loads(request.body)
            user_name = post_data.get("user_name", '')
            token = post_data.get("token", '')
            to_address = post_data.get("to_address", '')
            value = post_data.get("value", '')

            # Server Side Checks
            common_util.check_if_present(user_name, token, to_address, value)

            # Generate Address
            tx_hash = util.forward_batch(user_name, token, to_address, value)

            # Success
            return JsonResponse({'tx_hash': tx_hash, 'status': 200})

        except custom_exception.UserException as e:
            return JsonResponse({'error': str(e), 'status': 400})
        except Exception as e:
            util.init_logger()
            util.logger.info("Error get_fee : " + str(e))
            return JsonResponse({'error': exception_str.UserExceptionStr.bad_request, 'status' : 400})