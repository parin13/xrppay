from decimal import Decimal
import redis
import os, sys
from apscheduler.schedulers.blocking import BlockingScheduler
import datetime
from util import insert_sql, increment_sql, rpc_request, update_sql, send_notification, find_sql_join, config, MyLogger

# Redis Connection
pool = redis.ConnectionPool(
    host = config.get('redis', 'host'),
    port = int(config.get('redis', 'port')),
    db = int(config.get('redis', 'db'))
)
redis_conn = redis.Redis(connection_pool=pool)

# Blockchain Node
url = config.get('node', 'url')

logs_directory = config.get('ledger', 'logs')
category = config.get('ledger', 'category')
hook_queue = config.get('hook_main', 'queue')


def get_current_ledger(obj_logger):
    """
    RPC for getting the latest validated ledger
    :return:
    """
    ledger_info = rpc_request(obj_logger, 'ledger', [{"ledger_index": "validated"}])
    if not ledger_info:
        raise Exception('get_current_ledger | Node is Down')
    return int(ledger_info.get('result', {}).get('ledger', {}).get('ledger_index', 0))



def get_ledger_transactions(obj_logger, index):
    """
    RPC for reading ledger data
    :param index:
    :return:
    """

    transactions = rpc_request(obj_logger, 'ledger', [{"ledger_index": index, "transactions": True, "expand": True}])
    if not transactions:
        raise Exception('get_ledger_transactions | Node is Down')
    return transactions.get('result',{}).get('ledger',{}).get('transactions',[])


def validate_transaction(tx_result):
    """
    Check if the transaction is success or not
    :param tx_result:
    :return:
    """
    return tx_result == 'tesSUCCESS'


def receiver_crawler():
    """
    Main Process
    :return:
    """

    obj_logger = MyLogger(logs_directory, category)
    obj_logger.msg_logger('---------------------------- Start -------------------------------')
    obj_logger.msg_logger('Getting Ledger Numbers.....')

    # Get Current Block from RPC
    current_ledger = get_current_ledger(obj_logger)
    crawled_ledgers = int(redis_conn.get('xrp_ledger_crawled') or 0)

    obj_logger.msg_logger('Current Ledger Number : %s' % (current_ledger))
    obj_logger.msg_logger('Crawled Ledger Number : %s' % (crawled_ledgers))
    obj_logger.msg_logger('Pending : %s' % (current_ledger - crawled_ledgers))

    if current_ledger >= crawled_ledgers:

        # Crawling Ledgers
        for ledger_number in range(crawled_ledgers + 1, current_ledger + 1):

            obj_logger.msg_logger('Crawling Ledger : %s || Current Ledger : %s' % (ledger_number, current_ledger))
            obj_logger.msg_logger('Pending : %s' % (current_ledger - ledger_number))

            # Get Transaction Data
            transactions_list = get_ledger_transactions(obj_logger, ledger_number)

            for transaction_data in transactions_list:

                to_address = transaction_data.get('Destination','')

                if redis_conn.sismember('xrp_aw_set', to_address):

                    tx_hash = str(transaction_data['hash'])

                    if not redis_conn.sismember("xrp_vmp_set", tx_hash):

                        # Destination_Tag
                        obj_logger.msg_logger('>>>>>>>> Transaction Found : %s'%(tx_hash))

                        # Data
                        tx_result = transaction_data.get('metaData', {}).get('TransactionResult', '')
                        from_address = transaction_data.get('Account', '')
                        amount = transaction_data.get('Amount', '0')
                        destination_tag = str(transaction_data.get('DestinationTag', ''))
                        sequence = transaction_data.get('Sequence', '')

                        # Drop to XRP
                        drop_to_xrp = 1000000
                        amount = str(Decimal(int(amount)) / Decimal(drop_to_xrp))

                        if validate_transaction(tx_result):
                            status = 'Success'
                        else:
                            status = 'Failure'

                        # Insert in DB
                        data = {
                            'from_address': from_address,
                            'to_address': to_address,
                            'tx_hash': tx_hash,
                            'destination_tag': destination_tag,
                            'ledger_index': ledger_number,
                            'sequence': sequence,
                            'amount': amount,
                            'flag': 'Incoming',
                            'status': status,
                            'sys_timestamp': datetime.datetime.now()
                        }

                        success = insert_sql(
                            logger=obj_logger,
                            table_name='transaction_master',
                            data=data
                        )

                        redis_conn.sadd('xrp_vmp_set', tx_hash)

                        if success:
                            # Send Notification
                            notification_url = find_sql_join(logger=obj_logger,table_names=['user_master', 'address_master'],
                                                             filters={'address_master.address': to_address},
                                                             on_conditions={'user_master.user_name': 'address_master.user_name'},
                                                             columns=['user_master.notification_url'])
                            if notification_url:
                                notification_url = notification_url[0]['notification_url']
                                if notification_url:
                                    params = {
                                        'from_address': from_address,
                                        'to_address': to_address,
                                        'tx_hash': tx_hash,
                                        'destination_tag': destination_tag,
                                        'amount': amount
                                    }
                                    send_notification(obj_logger, notification_url, params, queue=hook_queue)
                                else:
                                    obj_logger.error_logger('>>>>>> Notification URL not found for %s ' % (tx_hash))
                            else:
                                obj_logger.error_logger('>>>>>> Notification URL not found for %s '%(tx_hash))

            redis_conn.set('xrp_ledger_crawled', ledger_number)
    obj_logger.msg_logger('---------------------------- Ends -------------------------------')

def main():
    try:
        receiver_crawler()
        # sched = BlockingScheduler(timezone='Asia/Kolkata')
        # sched.add_job(receiver_crawler, 'interval', id='my_job_id', seconds=10)
        # sched.start()
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print  error_stack
        obj_logger = MyLogger(logs_directory, category)
        obj_logger.error_logger('Main : %s' % (error_stack))


if __name__ == "__main__":
    main()
